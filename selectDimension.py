#! /usr/bin/env python
# -*- coding: utf-8 -*-

from collections import OrderedDict
from argparse import ArgumentParser, RawTextHelpFormatter
import numpy as np 
from ufz.netcdf4 import NcDataset


def setupParser():
    parser = ArgumentParser(
        description="Select indices along dimensions.",
        formatter_class=RawTextHelpFormatter)

    parser.add_argument('infile', help="Input file") 
    parser.add_argument('outfile', help="Output file")

    parser.add_argument("-d","--dims", nargs="+", help="dimension(s) to select")
    parser.add_argument("-i","--indices", nargs="+",
                        help=("0-based indices to select along given dimensions "
                              "\nExamples: "
                              "\n1,3,8   select the indices 1,3,8 "
                              "\n1/5     select the indices from 1 to 5 "
                              "\n1/15/2  select every second index between 1 and 15 "
                              "\n8/      select all indices starting from 8 "
                              "\n8//3    select every third index starting from 8 "
                              "\n/12     select all indices up to (but without) 12"
                              "\n/42/2   select every second index up to (but without 42)"
                              "\n//2     select every second index "))
    return parser


def parseIndex(idx):

    # a list of indices is given
    ivals = idx.split(",")
    if len(ivals) > 1:
        return map(int, ivals)

    # a range of indices
    ivals = idx.split("/")
    if len(ivals) > 1:
        ivals = [int(v) if v else None for v in ivals]
        if len(ivals) == 2:
            ivals.append(None)
        return slice(*ivals)

    # a single index
    return [int(idx)]
 

if __name__== "__main__":
    
    parser = setupParser()
    args = parser.parse_args()

    indices = [parseIndex(idx) for idx in args.indices]
    selslices = OrderedDict(zip(args.dims, indices))

    if len(args.dims) != len(indices):
        raise RuntimeError("Unequal number of arguments")

    with NcDataset(args.infile, "r") as ncin:

        with NcDataset(args.outfile, "w") as ncout:

            ncout.copyAttributes(ncin.attributes)

            for dname, dim in ncin.dimensions.items():
                dimvals = np.arange(len(dim))
                idx = selslices.get(dname, slice(None))
                ncout.createDimension(dname, len(dimvals[idx]))

            for vname, var in ncin.variables.items():
                newvar = ncout.copyVariable(var, data=False)
                idx = [selslices.get(d, slice(None)) for d in var.dimensions]
                newvar[:] = var[idx]
                    
