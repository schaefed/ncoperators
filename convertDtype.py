#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np 
from argparse import ArgumentParser
from ufz.netcdf4 import NcDataset


def setupParser():
    parser = ArgumentParser(
        description="Convert the data type(s) of the given variable(s).")

    parser.add_argument('infile', help="Input file") 
    parser.add_argument('outfile', help="Output file")

    parser.add_argument("-v","--vars", nargs="+", help="variable(s) to convert")
    parser.add_argument("-t","--types", nargs="+",
                        help="target data type(s) (e.g. float32, int64)")
    return parser


if __name__== "__main__":
    
    parser = setupParser()
    args = parser.parse_args()
    variables = args.vars
    dtypes = args.types

    if len(variables) != len(dtypes):
        raise RuntimeError("Unequal number of arguments")

    with NcDataset(args.infile, "r") as ncin:
        with NcDataset(args.outfile, "w") as ncout:
            ncout.copyDataset(ncin, skipvars=variables)
            for var, dtype in zip(variables, dtypes):
                invar = ncin.variables[var]
                invardef = invar.definition
                invardef["dtype"] = dtype
                try:
                    if invardef["fill_value"]:
                        invardef["fill_value"] = np.dtype(dtype).type(invardef["fill_value"])
                except TypeError:
                    pass
                outvar = ncout.createVariable(
                    invardef.pop("name"), invardef.pop("dtype"), **invardef)
                outvar.copyAttributes(invar.attributes)
                outvar[:] = invar[:]
